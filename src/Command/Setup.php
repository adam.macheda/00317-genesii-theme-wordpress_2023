<?php

namespace Genesii\Theme\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Filesystem\Filesystem;

#[AsCommand(
    name: 'setup',
    description: 'Génère les configurations et les fichiers nécessaires au fonctionnement du thème, ainsi que le nommage.',
    hidden: false
)]
class Setup extends Command {

    protected $vendor = null;
    protected $fs;

    public function __construct(?string $vendor) 
    {
        $this->vendor = $vendor;
        $this->fs = new Filesystem();

        parent::__construct();
    }

    public function configure(): void
    {
        $this
            ->addArgument('composerArgument1', InputArgument::OPTIONAL)
            ->addArgument('composerArgument2', InputArgument::OPTIONAL)
            ->addArgument('composerArgument3', InputArgument::OPTIONAL)
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $io->title('Assistant d\'installation');

        $nom = $io->askQuestion(new Question('<fg=green>Nom (affiché dans le back office)</>', 'Mon thème'));
        $description = $io->askQuestion(new Question('<fg=green>Description (affichée dans le back office)</>', 'Ma description'));
        $tags = $io->askQuestion(new Question('<fg=green>Tags (utilisés pour classer le thème)</>', 'theme personnalisé, genesii'));
        $version = $io->askQuestion(new Question('<fg=green>Numéro de version (affichée dans le back office)</>', '0.0.0'));

        $pathFichierTheme = $this->vendor . '/../style.css';

        $baseFichierTheme = file_get_contents($pathFichierTheme);

        $vendor = str_replace('/vendor', '', $this->vendor);
        $path = explode('/', $vendor);
        $slug = end($path);
        $slug = str_replace('-', '_', strtolower($slug));

        $baseFichierTheme = str_replace([
            '{wordpress_theme_name}',
            '{wordpress_theme_description}',
            '{wordpress_theme_version}',
            '{wordpress_theme_tags}',
            '{wordpress_theme_slug}'
        ], [$nom, $description, $version, $tags, $slug], $baseFichierTheme);

        $this->fs->dumpFile($pathFichierTheme, $baseFichierTheme);

        if($slug != 'wordpress_theme') $this->fs->rename($this->vendor . '/../translations/wordpress_theme.pot', $this->vendor . '/../translations/' . $slug . '.pot');
        $this->fs->remove([
            $this->vendor,
            $this->vendor . '/../composer.json',
            $this->vendor . '/../composer.lock',
            $this->vendor . '/../src',
        ]);

        $io->writeln('Thème généré ! Pour continuer, faire un <fg=yellow>yarn install</> dans le dossier <fg=yellow>assets</> puis activer le thème dans le back office.');
        $io->writeln('');
        $io->writeln('> Pour compiler les assets en mode <fg=yellow>développement</>, utiliser <fg=yellow>yarn run watch</> dans le dossier <fg=yellow>assets</>.');
        $io->writeln('> Pour compiler les assets en mode <fg=yellow>production</>, utiliser <fg=yellow>yarn run build</> dans le dossier <fg=yellow>assets</>.');
        $io->writeln('');

        return Command::SUCCESS;
    }
}