<?php

namespace Genesii\Theme\Command;

use Symfony\Component\Console\Application;

use Composer\Script\Event;

class Composer {

    public static function postInstall(Event $event) 
    {
        self::execute($event, Setup::class);
    }

    public static function maker(Event $event) 
    {
        self::execute($event, Maker::class);
    }

    public static function execute(Event $event, string $commandClass)
    {
        $vendor = $event->getComposer()->getConfig()->get('vendor-dir');

        $command = new $commandClass($vendor);

        $application = new Application();
        $application->add($command);
        $application->setDefaultCommand($command->getName(), true);
        $application->run();
    }
}